#ifndef _SIMPLE_VECTOR_HPP_
#define _SIMPLE_VECTOR_HPP_

#include <iostream>

//***************************************************************************************

template<typename Data>
class SimpleVector
{

//---------------------------------------------------

public:

	// Simple iterator struct
	struct Iterator
	{
		Data* m_pCurrentPosition;

		Iterator( Data* _pCurrentPosition )
			:m_pCurrentPosition( _pCurrentPosition )
		{}

		bool operator == ( const Iterator& _it )
		{
			return m_pCurrentPosition == _it.m_pCurrentPosition;
		}

		bool operator != ( const Iterator& _it )
		{
			return !( *this == _it );
		}

		Iterator& operator ++ ()
		{
			m_pCurrentPosition++;
			return *this;
		}

		Iterator& operator -- ()
		{
			m_pCurrentPosition--;
			return *this;
		}

		Data& operator * ()
		{
			return *m_pCurrentPosition;
		}
	};

	Iterator begin()
	{
		return Iterator( m_pData );
	}

	Iterator end()
	{
		return Iterator( m_pData + m_nUsed );
	}

//---------------------------------------------------

public:

	// Constructor, default size is 10
	SimpleVector( int _size = 10 );

	// Copy constructor
	SimpleVector( const  SimpleVector<Data>& _v );

	// Move constructor
	SimpleVector( SimpleVector<Data>&& _v );

	// Destructor
	~SimpleVector();

	// Copy assignment operator
	SimpleVector<Data>& operator = ( const SimpleVector<Data>& _v );

	// Move assignment operator
	SimpleVector<Data>& operator = ( SimpleVector<Data>&& _v );

	void clear();

	// Adding new element into the back of the vector
	void pushBack( const Data& _data );

	// Adding new element into the beginning of the vector
	void pushFront( const Data& _data );

	// Adding new element into position, pointed by iterator
	template<typename It>
	void insertAt( It _position, const Data& _data );

	// Deleting the last element
	void popBack();

	// Deleting the first element
	void popFront();

	// Deleting an element by the position, pointed by iterator
	template<typename It>
	void erase( It _position );

	// Checking if the vector is empty
	bool isEmpty() const;

	bool operator == ( const SimpleVector& _vec ) const;

	bool operator != ( const SimpleVector& _vec ) const;

	// Returning an element which can be modified
	Data& operator[]( int _position);
	
	// Returns current number of elements, vector contains
	int getSize() const;

	// Returns the size of the storage space currently allocated for the vector
	int getCapacity() const;

//---------------------------------------------------

private:

	// Method, increase the size of allocation number by 2 times
	void grow();

	// Method, reduce the size of allocation number by 2 times unless it's already less than 20 
	void shrink();

	// Method, change the size of allocation number
	void resize( int _newSize );

	// Checking if the number of a free space is 2 times bigger than we use atm
	bool isAllocatedTwoTimesBiggerUsed() const;

//---------------------------------------------------

private:

	int m_nAllocated;
	int m_nUsed;
	Data* m_pData;

//---------------------------------------------------

};

//***************************************************************************************

template<typename Data>
SimpleVector<Data>::SimpleVector( int _size )
	:m_nAllocated( _size ), m_nUsed( 0 )
{
	m_pData = new Data[m_nAllocated];
}

//***************************************************************************************

template<typename Data>
SimpleVector<Data>::SimpleVector( const SimpleVector<Data>& _v )
	:m_nUsed( _v.m_nUsed ), m_nAllocated( _v.m_nAllocated )
{
	m_pData = new Data[m_nAllocated];

	const rsize_t size = sizeof( Data ) * m_nUsed;

	memcpy_s( m_pData, size, _v.m_pData, size );
}

//***************************************************************************************

template<typename Data>
SimpleVector<Data>::SimpleVector( SimpleVector<Data>&& _v )
	:m_nUsed(_v.m_nUsed), m_nAllocated(_v.m_nAllocated)
{
	m_pData = _v.m_pData;
	_v.m_pData = nullptr;
	_v.m_nUsed = 0;
}

//***************************************************************************************

template<typename Data>
SimpleVector<Data>& SimpleVector<Data>::operator = ( const SimpleVector<Data>& _v )
{
	if( this == &_v )
		return *this;

	delete[] m_pData;

	m_nUsed = _v.m_nUsed;
	m_nAllocated = _v.m_nAllocated;

	m_pData = new Data[m_nAllocated];

	const rsize_t size = sizeof( Data ) * m_nUsed;

	memcpy_s( m_pData, size, _v.m_pData, size );
}

//***************************************************************************************

template<typename Data>
SimpleVector<Data>& SimpleVector<Data>::operator = ( SimpleVector<Data>&& _v )
{
	if( this == &_v )
		return *this;

	delete[] m_pData;

	m_nUsed = _v.m_nUsed;
	m_nAllocated = _v.m_nAllocated;

	m_pData = new Data[m_nAllocated];

	const rsize_t size = sizeof( Data ) * m_nUsed;

	memcpy_s( m_pData, size, _v.m_pData, size );

	return *this;
}

//***************************************************************************************

template<typename Data>
SimpleVector<Data>::~SimpleVector()
{
	delete[] m_pData;
}

//***************************************************************************************

template<typename Data>
void SimpleVector<Data>::clear()
{
	delete[] m_pData;

	m_nAllocated = 10;
	m_nUsed = 0;

	m_pData = new Data[m_nAllocated];	
}

//***************************************************************************************

template<typename Data>
void SimpleVector<Data>::pushBack( const Data& _data )
{
	if( m_nUsed == m_nAllocated )
		grow();
	m_pData[m_nUsed++] = _data;
}

//***************************************************************************************

template<typename Data>
void SimpleVector<Data>::pushFront( const Data& _data )
{
	if( isEmpty() )
	{
		pushBack( _data );
		return;
	}

	if( m_nUsed == m_nAllocated )
		grow();

	for( int i = m_nUsed; i >= 0; i-- )
		m_pData[i + 1] = m_pData[i];

	m_pData[0] = _data;
	++m_nUsed;
}

//***************************************************************************************


template<typename Data>
template<typename It>
void SimpleVector<Data>::insertAt( It _position, const Data& _data )
{
	if( m_nUsed == m_nAllocated )
	{
		int position = _position.m_pCurrentPosition - begin().m_pCurrentPosition;
		grow();
		_position = Iterator ( m_pData + position );
	}

	Iterator current = end();
	Iterator previous = --end();

	while( current != _position )
	{
		*current = *previous;
		--current;
		--previous;
	}

	*_position = _data;
	++m_nUsed;
}

//***************************************************************************************

template<typename Data>
void SimpleVector<Data>::resize( int _newSize )
{
	Data* pTemp = new Data[_newSize];

	const rsize_t size = sizeof( Data ) * m_nUsed;

	memcpy_s( pTemp, size, m_pData, size );

	delete[] m_pData;

	m_pData = pTemp;
	m_nAllocated = _newSize;
}

//***************************************************************************************

template<typename Data>
void SimpleVector<Data>::grow()
{
	int newSize = m_nAllocated << 1;
	resize( newSize );
}

//***************************************************************************************

template<typename Data>
void SimpleVector<Data>::shrink()
{
	int newSize = m_nAllocated >> 1;
	resize( newSize );
}

//***************************************************************************************

template<typename Data>
void SimpleVector<Data>::popBack()
{
	if( isEmpty() )
		return;

	m_pData[m_nUsed--].~Data();

	if( isAllocatedTwoTimesBiggerUsed() )
		shrink();
}

//***************************************************************************************

template<typename Data>
void SimpleVector<Data>::popFront()
{
	if( isEmpty() )
		return;

	for( int i = 0; i < m_nUsed - 1; i++ )
	{
		m_pData[i] = m_pData[i + 1];
	}

	popBack();
}

//***************************************************************************************

template<typename Data>
template<typename It>
void SimpleVector<Data>::erase( It _position )
{
	if( isEmpty() )
		throw std::runtime_error( "Can't erase an element from empty vector" );
	
	if( _position == --end() )
	{
		popBack();
		return;
	}
	else if( _position == begin() )
	{
		popFront();
		return;
	}

	Iterator current = _position;
	Iterator next = ++_position;

	while( current != end() )
	{
		*current = *next;
		++current;
		++next;
	}

	popBack();
	return;

}

//***************************************************************************************

template<typename Data>
inline bool SimpleVector<Data>::isEmpty() const
{
	return ( m_nUsed == 0 );
}

//***************************************************************************************

template<typename Data>
bool SimpleVector<Data>::operator == ( const SimpleVector& _vec ) const
{
	if( getSize() != _vec.getSize() )
		return false;

	return !memcmp( m_pData, _vec.m_pData, ( getSize() * sizeof( Data ) ) );
}

//***************************************************************************************

template<typename Data>
bool SimpleVector<Data>::operator != ( const SimpleVector& _vec ) const
{
	return !( *this == _vec );
}

//***************************************************************************************

template<typename Data>
Data& SimpleVector<Data>::operator []( int _position )
{
	if( _position >= m_nUsed || _position < 0 )
		throw std::out_of_range( "position is out of range" );
		

	return m_pData[_position];
}

//***************************************************************************************

template<typename Data>
inline bool SimpleVector<Data>::isAllocatedTwoTimesBiggerUsed() const
{
	return ( ( m_nUsed <= ( m_nAllocated / 2 ) ) && ( m_nAllocated >= 20 ) );
}

//***************************************************************************************

template<typename Data>
inline int SimpleVector<Data>::getSize() const
{
	return m_nUsed;
}

//***************************************************************************************

template<typename Data>
inline int SimpleVector<Data>::getCapacity() const
{
	return m_nAllocated;
}

//***************************************************************************************


#endif //_SIMPLE_VECTOR_HPP_
